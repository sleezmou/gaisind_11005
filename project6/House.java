import java.util.concurrent.TimeUnit;
public class House {
    String status;
    TechTask techTask;
    int builders;
    int engineers;
    int painters;
    int planners;
    public House(TechTask techTask) {
        this.techTask = techTask;
        if (this.status == null)
            this.status = "Не строится";
    }
    public void buildingTimer() {
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            this.status = "Построен";
        }

    }
    public void printOrder(){
        System.out.println("Заказ номер " + techTask.orderNumber);
        System.out.println("Статус заказа: " + this.status);
        System.out.println("Техзадание:");
        System.out.println(techTask.floors + " - кол-во этажей");
        if (techTask.elevator == 1)
            System.out.println("Есть лифт");
        else
            System.out.println("Нет лифта");
        System.out.println(techTask.entrances + " - кол-во поъездов");
        System.out.println(techTask.roomArea + " - плошадь комнат");
        if(techTask.color.length() > 1)
            System.out.println(techTask.color + " - цвет дома");
        else
            System.out.println("Дом бесцветный");
        System.out.println();
        if (builders >= 1) {
            System.out.println("Бригада:");
            System.out.println(this.builders + " строителей");
            System.out.println(this.engineers + " инженеров");
            if (this.painters != 0)
                System.out.println(this.painters + " мяляров");
            System.out.println(planners + " планировщиков");
            System.out.println();
        }
    }
}
