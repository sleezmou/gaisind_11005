import java.util.Scanner;
public class Menu {
    Scanner sc = new Scanner(System.in);
    int button;
    public Menu() {
    }
    public void setButton() {
        System.out.println("Нажмите 1, если хотите добавить новое техзадание на постройку дома");
        System.out.println("Нажмите 2, если хотите посмотреть список всех заказов");
        System.out.println("Нажмите 3, если хотите назначить бригаду рабочих");
        System.out.println("Нажмите 4, если хотите выйти");
        this.button = sc.nextInt();
    }
    public int getButton() {
        return this.button;
    }
}
