import java.util.Scanner;
public class TechTask {
    Scanner sc = new Scanner(System.in);
    int orderNumber;
    int floors;
    int entrances;
    public int elevator;
    int roomArea;
    String color;
    public TechTask() {
    }
    public void setFloors() {
        System.out.println("Введите нужно кол-во этажей");
        this.floors = sc.nextInt();
    }
    public void setEntrances(){
        System.out.println("Введите нужное кол-во подъездов");
        this.entrances = sc.nextInt();
    }
    public void setElevator(){
        System.out.println("Введите 1, если вам нужен лифт, 0 - если нет");
        this.elevator = sc.nextInt();
    }
    public void setRoomArea() {
        System.out.println("Введите нужную площадь комнат");
        this.roomArea = sc.nextInt();
    }
    public void setColor() {
        System.out.println("Введите нужный вам цвет дома. Если же вы хотите оставить дом бесцветным - введите 0");
        this.color = sc.nextLine();
    }
}
