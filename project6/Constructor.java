import java.util.ArrayList;
import java.util.Scanner;
public class Constructor {
    Scanner sc = new Scanner(System.in);
    public ArrayList<House> houses = new ArrayList<House>();

    public Constructor() {
    }

    int i = 0;

    public void handleMenuButton() {
        Menu menu = new Menu();
        while (menu.button != 4) {
            menu.setButton();
            int a = menu.getButton();
            switch (a) {
                case 1:
                    TechTask techTask = new TechTask();
                    techTask.setColor();
                    techTask.setElevator();
                    techTask.setEntrances();
                    techTask.setFloors();
                    techTask.setRoomArea();
                    houses.add(new House(techTask));
                    techTask.orderNumber = i;
                    calculatePrice(techTask.entrances, techTask.floors, techTask.roomArea);
                    System.out.println();
                    this.i += 1;
                    break;
                case 2:
                    if (houses.size() > 0) {
                        for (int j = 0; j < houses.size(); j++)
                            houses.get(j).printOrder();
                        System.out.println();
                    } else {
                        System.out.println("Пока нет ни одного заказа");
                        System.out.println();
                    }
                    break;
                case 3:
                    System.out.println("Введите номер заказа, к которому хотите приставить бригаду рабочих:");
                    int orderNumber = sc.nextInt();
                    try {
                        if (houses.get(orderNumber).builders == 0) {
                            houses.get(orderNumber).builders = 1 + (houses.get(orderNumber).techTask.floors + (int) houses.get(orderNumber).techTask.roomArea) * 3;
                            houses.get(orderNumber).engineers = 1 + (houses.get(orderNumber).techTask.elevator + houses.get(orderNumber).techTask.floors + (int) houses.get(orderNumber).techTask.roomArea) / 2;
                            houses.get(orderNumber).planners = 1 + houses.get(orderNumber).techTask.floors / 3;
                            if (houses.get(orderNumber).techTask.color.length() > 1)
                                houses.get(orderNumber).painters = 1 + (houses.get(orderNumber).techTask.floors + houses.get(orderNumber).techTask.entrances) * 2;
                            else
                                houses.get(orderNumber).painters = 0;
                            System.out.println("Бригада рабочих назначена. Информацию о ней в можете посмотреть в списке заказов");
                            System.out.println();
                            houses.get(orderNumber).buildingTimer();
                            houses.get(orderNumber).status = "Построен";
                            System.out.println("Ваш дом построен!");
                            System.out.println();
                        } else {
                            System.out.println("К введённому вами номеру заказа уже назначена бригада");
                            System.out.println();
                        }
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("Заказа с введённым вами номером не существует");
                        System.out.println();
                    }
            }
        }
    }
    public void calculatePrice(int entrances, int floors, double roomArea){
        double price = entrances * floors * roomArea;
        System.out.println("Цена строительства: " + price + " тысяч рублей");
    }
}