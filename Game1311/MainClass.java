public class MainClass {
    public static void main (String[] args) {
        Player player1 = new Player("Artur", 20);
        Player player2 = new Player("Danil", 30);
        Game game = new Game();
        while (player1.hp * player2.hp > 0) {
            game.attack(1, player1, player2);
            game.attack(2, player1, player2);
        }
    }
}
