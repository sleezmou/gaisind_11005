import java.util.Random;
import java.util.Scanner;
public class Game {
    Random random = new Random();
    Scanner scanner = new Scanner(System.in);
    public void attack( int turn, Player player1, Player player2) {
        if (turn == 1) {
            System.out.println();
            System.out.println("Now it's " + player1.name + "'s turn. Enter damage");
            int damage = scanner.nextInt();
            int c = random.nextInt(11);
            if (c > damage) {
                player2.hp -= damage;
            }
            System.out.println("Now " + player2.name + "'s hp = " + player2.hp);
            if (player2.hp < 1)
                System.out.println(player1.name + " is a winner!");
        } else {
            System.out.println();
            System.out.println("Now it's " + player2.name + "'s turn. Enter damage");
            int damage = scanner.nextInt();
            int c = random.nextInt(11);
            if (c > damage) {
                player1.hp -= damage;
            }
            System.out.println("Now " + player1.name + "'s hp = " + player1.hp);
            if (player1.hp < 1)
                System.out.println(player2.name + " is a winner!");
        }
    }
}
