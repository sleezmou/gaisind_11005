//Нахождение самого длинного слова в строке
import java.util.Scanner;
public class StringLength {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int i = 0;
        int maxCount = 0;
        int count = 0;
        String line = "";
        String maxLength = "";
        String mainLine = in.nextLine();
        for (i = 0; i < mainLine.length(); i++) {
            count = count + 1;
            line = line + mainLine.charAt(i);
            if (mainLine.charAt(i) == ' ') {
                if (count >= maxCount) {
                    maxCount = count;
                    maxLength = line;
                }
                count = 0;
                line = "";
            }
        }
        System.out.println(maxLength);
    }
}