public class Order {
    Client client;
    Product product;
    public Order(Client client, Product product) {
        this.client = client;
        this.product = product;
    }
    public void orderPrint() {
        System.out.println("Имя клиента: " + client.clientName + "; Название продукта: " + product.productName);
    }
}
