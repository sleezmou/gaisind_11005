import java.util.Scanner;
public class Client {
    Scanner scanner = new Scanner(System.in);
    public String clientName;
    public String sex;
    public int age;
    public Client() {
        System.out.println("Введите имя клиента");
        this.clientName = scanner.nextLine();
        System.out.println("Введите пол клиента");
        this.sex = scanner.nextLine();
        System.out.println("Введите возраст клента");
        this.age = scanner.nextInt();
    }
}
