import java.util.Scanner;
public class Product {
    Scanner scanner = new Scanner(System.in);
    public String productName;
    public String producer;
    public int price;
    public Product() {
        System.out.println("Введите название продукта");
        this.productName = scanner.nextLine();
        System.out.println("Введите производителя продукта");
        this.producer = scanner.nextLine();
        System.out.println("Введите цену продукта");
        this.price = scanner.nextInt();
    }
}
