import java.util.ArrayList;
import java.util.Scanner;
public class MainClass {
    static Scanner scanner = new Scanner(System.in);
    public static void main (String args[]) {
        int i = 0;
        int menu = 4;
        ArrayList<Client> clients = new ArrayList<Client>();
        ArrayList<Product> products = new ArrayList<Product>();
        ArrayList<Order> orders = new ArrayList<Order>();
        while (menu != 3) {
        System.out.println("Введите 1, если хотите добавить клиента и заказ\nВведите 2, если хотите вывести весь список заказов \nВведите 3, если хотите выйти");
            menu = scanner.nextInt();
            if (menu == 1) {
                clients.add(new Client());
                products.add(new Product());
                orders.add(new Order(clients.get(i), products.get(i)));
                i += 1;
            }
            for (int j = 0; (menu == 2) && j < orders.size(); j++) {
                orders.get(j).orderPrint();
            }
            System.out.println("\n \n");
        }
    }
}
