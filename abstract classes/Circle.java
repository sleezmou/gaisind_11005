public class Circle extends Shape {
    private double area;
    public Circle(double r) {
        this.area = r * r * Math.PI;
    }
    public double getArea() {
        return this.area;
    }
}
