public class Triangle extends Shape {
    private double area;
    public Triangle(double a, double h) {
        this.area = (a * h) / 2;
    }
    public double getArea() {
        return this.area;
    }
}
