public class Main {
    public static void main(String[] args) {
        Shape[] shape = new Shape[5];
        shape[0] = new Rectangle(1, 2);
        shape[1] = new Circle(3);
        shape[2] = new Triangle(4.5, 6);
        shape[3] = new Circle(7);
        shape[4] = new Triangle(8, 9);
        for (int i = 0; i < 5; i++) {
            System.out.println(shape[i].getArea());
        }
    }
}
