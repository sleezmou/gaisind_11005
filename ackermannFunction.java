//Функция Аккермана
public class ackermannFunction {
    public static int ackerFunc (int m, int n) {
        if (m == 0)
            return n + 1;
        else if (n == 0)
            return ackerFunc(m - 1, 1);
        else
            return ackerFunc(m - 1, ackerFunc(m, n - 1));
    }
    public static void main (String args[]) {
        int n = 3;
        int m = 3;
        int a = ackerFunc(m, n);
        System.out.println(a);
    }
}