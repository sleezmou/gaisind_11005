//Найти определитель 2х2 и 3х3
import java.util.Scanner;
public class determinant {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter matrix's side's length (2 or 3)");
        int i = in.nextInt();
        int j = i;
        int det = 0;
        int[][] matrix = new int[i][j];
        for (i = 0; i < matrix.length; i++) {
            for (j = 0; j < matrix.length; j++) {
                matrix[i][j] = in.nextInt();
            }
        }
       if (i == 2) {
           det = matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
       }
       else {
           det = matrix[0][0] * matrix[1][1] * matrix[2][2] + matrix[0][1] * matrix[1][2] * matrix[2][0] + matrix[0][2] * matrix[1][0] * matrix[2][1] - matrix[0][0] * matrix[1][2] * matrix[2][1] - matrix[0][2] * matrix[1][1] * matrix[2][0] - matrix[0][1] * matrix[1][0] * matrix[2][2];
       }
        System.out.println(det);
    }
}