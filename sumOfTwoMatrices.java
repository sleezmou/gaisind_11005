//Сумма двух матриц
import java.util.Random;
public class sumOfTwoMatrices {
    public static void sumUpMatrices(int[][] matrix1, int[][] matrix2, int i, int j, int n) {
        int[][] summedUpMatrix = new int[i][j];
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                summedUpMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                System.out.print(summedUpMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }
    public static void main(String args[]){
        Random random = new Random();
        int n = 3;
        int i = n;
        int j = n;
        int[][] matrix1 = new int[i][j];
        int[][] matrix2 = new int[i][j];
        for (i = 0; i < n; i++){
            for (j = 0; j < n; j++){
                matrix1[i][j] = random.nextInt(100);
                System.out.print(matrix1[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        for (i = 0; i < n; i++){
            for (j = 0; j < n; j++){
                matrix2[i][j] = random.nextInt(100);
                System.out.print(matrix2[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
        sumUpMatrices(matrix1, matrix2, i, j, n);
    }
}