public class Vector2D {
    public double x;
    public double y;
    public void setXY (double x, double y) {
        this.x = x;
        this.y = y;
    }
    public void add (double x1, double x2, double y1, double y2) {
        this.x = x1 + x2;
        this.y = y1 + y2;
    }
    public void sub (double x1, double x2, double y1, double y2) {
        this.x = x1 - x2;
        this.y = y1 - y2;
    }
    public void mult(double x, double y, double t) {
        this.x = x * t;
        this.y = y * t;
    }
}
