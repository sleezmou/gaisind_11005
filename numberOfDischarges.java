//Нахождение кол-ва разрядов числа
public class numberOfDischarges{
    public static void main(String args[]){
        int number = 123;
        System.out.println(getNumberOfDischarges(number));
    }
    public static int getNumberOfDischarges(int number){
        int numberOfDischarges = 0;
        while (number >= 1) {
            numberOfDischarges = numberOfDischarges + 1;
            number = number / 10;
        }
        return numberOfDischarges;
    }
}