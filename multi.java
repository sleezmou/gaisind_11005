//Умножение матриц
import java.util.Scanner;
public class multi {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the length of the matrix: ");
        int b = in.nextInt();
        System.out.print("Enter the width of the matrix: ");
        int a = in.nextInt();
        int sum = 0;
        int[][] mas1 = new int[a][b];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < mas1[i].length; j++) {
                mas1[i][j] = ((int)(Math.random() * 100));
            }
        }
        System.out.println("First matrix: ");
        for (int[] ints : mas1) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println(" ");
        }
        int[][] mas2 = new int[b][a];
        for (int i = 0; i < mas2.length; i++) {
            for (int j = 0; j < mas2[i].length; j++) {
                mas2[i][j] = ((int)(Math.random() * 100));
            }
        }
        System.out.println("Second matrix: ");
        for (int[] ints : mas2) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println(" ");
        }
        int[][] res = new int[a][a];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                for (int k = 0; k < b; k++) {
                    sum = sum + mas1[i][k] * mas2[k][j];
                }
                res[i][j] = sum;
                sum = 0;
            }
        }
        System.out.println("Result matrix: ");
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                System.out.print(res[i][j] + " ");
            }
            System.out.println(" ");
        }
    }
}