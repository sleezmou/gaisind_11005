//Склеивание двух массивов
public class gluedArrays {
    public static void glueArrays(int[] mas1, int[] mas2) {
        int i = 0;
        int[] gluedArray = new int[mas1.length + mas2.length];
        for (i = 0; i < (mas1.length); i++) {
            gluedArray[i] = mas1[i];
            gluedArray[i + mas1.length] = mas2[i];
        }
        for (i = 0; i < (mas1.length) + (mas2.length); i++) {
            System.out.print(gluedArray[i] + " ");
        }
    }
    public static void main (String args[]) {
        int[] mas1 = {1, 2, 3, 4};
        int[] mas2 = {5, 6, 7, 8};
        glueArrays(mas1, mas2);
    }

}