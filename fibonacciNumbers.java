//Числа Фибоначчи. Вычисление элемента с номером N
public class fibonacciNumbers {
    public static int fiboNums (int n) {
        if (n == 1)
            return 0;
        else if (n == 2)
            return 1;
        else
            return fiboNums(n - 1) + fiboNums(n - 2);
    }
    public static void main (String args[]) {
        int n = 6;
        int answer = fiboNums(n);
        System.out.println(answer);
    }
}