//Вывод числа от A до B через рекурсию
public class outputNumber {
    public static void outNum(int n) {
        if (n != 1)
            outNum(n - 1);
        System.out.print(n + " ");
    }
    public static void main (String args[]) {
        int n = 5;
        outNum(n);
    }
}