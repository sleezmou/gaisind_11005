//Умножение цифр числа через рекурсию
public class numberMultiplication {
    public static int multiplication (int n) {
        if (n < 1) {
            return 1;
        }
        return n * multiplication(n - 1);
    }
    public static void main (String args[]) {
        int n = 25;
        int multi = multiplication(n);
        System.out.println(multi);
    }
}