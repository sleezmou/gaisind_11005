//Нахождение максимального элемента в массиве
public class maxNumberOfArray{
    public static void main(String args[]){
        int[] mas = new int[] {1,5,8,6,0,3,987,6546,987,1234,7657};
        System.out.println(maxNumberFinder(mas));
    }
    public static int maxNumberFinder(int[] mas){
        int i = 0;
        int max = mas[0];
        for(i = 0; i < mas.length; i++){
            if(mas[i] > max){
                max = mas[i];
            }
        }
        return max;
    }
}